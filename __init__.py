# -*- coding: utf-8 -*-

from trytond.pool import Pool
from .health_ward import *
from .report import *


def register():
  Pool.register(
    RegistroGuardia,
    module='health_ward_fiuner', type_='model')
  Pool.register(
    RegistroGuardias,
    module='health_ward_fiuner', type_='report')

